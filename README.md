# Design on the Inside

Design on the Inside uncovers how organisations design the products and services that we use everyday. We explore the ins and outs of how design is being implemented, used, and scaled across organisations. This is it's website [https://designontheinside.com](https://designontheinside.com).

## Install

`brew install hugo`

## Clone

```
git clone https://gitlab.com/doti/doti.gitlab.io
cd doti.gitlab.io
```

## Run

`hugo server`

Visit [localhost:1313](localhost:1313) in your browser.
