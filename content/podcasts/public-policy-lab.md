---
title: Public Policy Lab
date: 2018-05-16 05:39:10 +0000
type: podcasts
description: Shanti is helping Governments design better services in the states. We
  talk about the role that agencies play in helping institutions like the New York
  state department take design seriously and think about user needs.
speakers:
- image: "/uploads/2018/05/16/Shanti Mathew003_cropped_small.jpeg"
  firstName: Shanti
  lastName: Mathew
  role: Strategy Director
  company:
    name: Public Policy Lab
    URL: http://publicpolicylab.org/
  bio: As Deputy Director of the Public Policy Lab, Shanti partners with at-risk communities
    and government agencies to make critical public services easier to access, more
    effective to operate, and more enjoyable to use. Before joining the Public Policy
    Lab, Shanti led business development for a tech startup and managed the production
    of large-scale events. Her prior innovation research and strategy consulting has
    spanned the public, social, and private sectors. She holds degrees from the University
    of Wisconsin-Madison and the Illinois Institute of Technology.
  links:
  - title: Twitter
    URL: https://twitter.com/ShantiMMathew
quote: ''
runtime: ''
draft: true
---
