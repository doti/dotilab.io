---
title: Barnardos
date: 2018-05-16 05:35:09 +0000
type: podcasts
description: We talk to Audree, who’s heading up a new capability within Children’s
  charity Barnardos, Service Design. Audrey talks to us about building their internal
  design products, like recruiting.
quote: We might think we’re stretched, but that’s nothing compared to what our frontline
  colleagues are feeling. It’s hard to get time with them.
speakers:
- image: "/uploads/2018/05/16/Audree Fletcher.jpg"
  firstName: Audree
  lastName: Fletcher
  company:
    name: Barnardos
  bio: "Audree is Head of Service Design for Barnardos, leading the team that designs
    and builds services for some of the country's most vulnerable children.  \n\nAfter
    a long career as a policy advisor in Government, Audree pivoted to develop expertise
    in research, design and strategy for transformation. She built a service innovation
    and insight team within UK Trade and Investment, had a brief stint as Chief Digital
    Officer there, before becoming Director of Design in the Department for International
    Trade."
  links:
  - title: Twitter
    URL: https://twitter.com/avfletcher
  role: Head of Service Design
runtime: ''
draft: true
---
