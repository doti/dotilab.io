---
title: Coloplast
date: 2018-05-16 05:25:54 +0000
type: podcasts
quote: There’s so much knowledge you get from being inside an organisation, but being
  on the inside you need to make sure you can still challenge the mindset!
description: We talk to Lillith from Coloplast and their journey from inventors of
  the colostomy bag to moving beyond products into services. Listen in to hear Lillith
  talk about how research plays a vital role in their innovation journey.
speakers:
- image: "/uploads/2018/05/16/Lilith_headshot500x500.jpg"
  firstName: Lillith
  lastName: Hasgaard
  role: User Researcher
  company:
    name: Coloplast
  links:
  - title: Website
    URL: http://www.hasbeck.com/
  - title: LinkedIn
    URL: https://www.linkedin.com/in/lilithhasbeck
  - title: Twitter
    URL: https://twitter.com/lilithhasbeck
  bio: After having finished her postgraduate studies in Service Design at the Royal
    College of Art in London in 2016, Lilith headed home to Copenhagen, where she’s
    now working as user researcher at Coloplast (a Danish medtech company). Prior
    to service design, her background was in visual communication (graphic design)
    and co-design (design anthropology). She’s previously worked at Livework (an East
    London service design agency), Policy Lab (at the Cabinet Office), and MindLab
    (a cross-governmental innovation lab in Copenhagen).  Within these different settings,
    she’s worked with service design, design research, visual communication, and bits
    of policy design. In practice, much of her work has centred on aspects of healthcare
    provision.
runtime: ''
draft: true
---
