---
title: Hive
date: 2018-05-16 05:30:39 +0000
type: podcasts
description: We talk to Nicola, who is an expert in Thermostats, heading up Hive’s
  product development. Hear from Nicola on how she manages both the constant need
  for innovation and iterating their existing products based on user feedback.
quote: We are really focussed on our customers, knowing who they are and how they
  live their lives. We try and talk to them as often as we can.
speakers:
- image: "/uploads/2018/05/16/BW5T9771.JPG"
  firstName: Nicola
  lastName: Combe
  role: Global Product Lead
  company:
    name: Hive
    URL: https://www.hivehome.com
  links:
  - title: Twitter
    URL: https://twitter.com/niccombe
  bio: Nicola is the global product lead for Hive Active Heating™ at Connected Homes.
    She’s been working finishing her doctorate and now develops environmental technologies
    that can be controlled remotely for markets across the globe. Nicola is responsible
    for managing the whole lifecycle of the product from scoping of a feature or product;
    understanding customers’ needs; and implementing the appropriate engineering solutions.
runtime: 46
soundcloudURL: ''
iTunesURL: ''
draft: true
---
