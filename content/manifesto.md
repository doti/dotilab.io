---
title: Manifesto
date: 2018-05-01 11:59:16 +0000
type: page
slug: manifesto
layout: manifesto
description: |-
  We set up Design on the inside to uncover the reality of how organisations design the products and services that we use everyday. We felt that only the 'hero' stories of design were being told and it was time to shine a light on the often difficult journey organisations and individuals face when trying to design great things.

  We strive for honest conversations that reveal the real side of making great design happen.
manifesto_blocks:
- template: manifesto-block
  image_position: Left
  image: "/uploads/2018/09/03/manifesto_illustration_1.svg"
  image_alt_text: Beardy high five
  copy: Design on the inside started out as a bi-monthly breakfast event at Snook’s
    London Studio. We invited people who both identified with design, and those who
    didn’t, to talk openly and honestly about their journey and challenges they face.
- template: manifesto-block
  image_position: Right
  copy: The sessions grew in numbers and continued to attract people interested in
    how design can be used to make products and services that work for people. DOTI
    seemed to naturally create a trusting space. Speakers continued to be honest,
    their lessons often frank and their unsolved problems voiced.
  image: "/uploads/2018/09/03/manifesto_illustration_2.svg"
  image_alt_text: Peanut and a haircut
- template: manifesto-banner
  image: "/uploads/2018/09/03/manifesto_illustration_3.svg"
  image-alt-text: Bacon and eggs
- template: manifesto-block
  image_position: Right
  copy: We’ve always felt there was a lot of great material missing from the industry
    on this unseen, often unsexy side of making design work. And we couldn’t let these
    honest lessons be left just to the walls of our sessions.  Over time, we built
    a podcast to bring these conversations to more people, and shine a light on the
    heroes who are doing the handwork to make things work better for people.
sponsor: Powered by Snook
sponsor_copy: "[Snook](https://wearesnook.com) is an award-winning design agency,
  based in Glasgow and London. We’re on a mission to design a world that works better
  for people. We specialise in designing exceptional experiences and building the
  design capacity of organisations."
sponsor_logo: "/uploads/2018/07/27/Snook logo_White_no TM.svg"
menu:
  main:
    weight: 3
---
## How it all began
