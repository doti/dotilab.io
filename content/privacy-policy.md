---
title: Privacy Policy
date: 2018-05-24 14:34:47 +0000
type: page
template: single
description: This is the privacy policy for Design on the Inside. We wrote it with
  clarity and brevity in mind and does not provide exhaustive detail of all aspects
  of our collection and use of personal information. We’re happy to provide any additional
  information or explanation needed beyond that. The privacy notice is valid from
  25th May 2018. We keep our privacy notice under regular review, it was last updated
  in May 2018.
slug: ''
content:
- template: generic-content-block
  header: How we use your information
  copy: |-
    This privacy notice tells you what to expect when Design on the Inside (operated by We Are Snook Limited (aka Snook)) collects personal information. It applies to information we collect about:

     * people who visit our website and social media channels
     * people who email us
     * people who use our services (including our newsletter)

     Note that people who participate in our research or apply to work with us will receive an extended version of our privacy notice. This privacy notice does not apply to businesses or organisations.

     Under the General Data Protection Regulation individuals have the right to make a request for information, rectification, erasure, restriction, transfer, and/or object to the processing of their data. If this concerns you, please see more information about your rights and how to use them below.
- template: generic-content-block
  header: People who visit our website
  copy: |-
    We use a third-party service, gitlab.com, to host our website designontheinside.com.
    When someone visits our website we use a third party service, Google Analytics, to collect standard internet log information and details of visitor behaviour patterns. We do this to find out things such as the number of visitors to the various parts of the site. This information is processed in a way that does not identify anyone. We do not make, and do not allow Google to make, any attempt to find out the identities of those visiting our website. If we do want to collect personally identifiable information through our website, we will be up front about this and ask for your explicit consent. We will make it clear when we collect personal information and will explain what we intend to do with it.
    Our website uses cookies to save browsing preferences and to optimise users’ browsing experience. You can read more about how we use cookies in our [**cookies policy**](https://www.iubenda.com/privacy-policy/7869317/cookie-policy).
    Snook uses a third party service to help maintain the security and performance of its website. To deliver this service it processes the IP addresses of visitors to the website.
    This privacy notice does not cover the links within this site linking to other websites. We encourage you to read the privacy statements on the other websites you visit.
- template: generic-content-block
  header: People who email us
  copy: We use a third party provider, Gmail, for our email services. Our provider
    uses a Secure Sockets Layer (SSL) to encrypt and protect email traffic.  Messages
    are sent using SSL between Snook and Gmail. However, you should be aware that
    any emails we send or receive may not be protected in transit. We will also monitor
    any emails sent to us, including file attachments, for viruses or malicious software.
- template: generic-content-block
  copy: Snook offers certain services to the public, such as subscribing to our e-newsletter.
    We have to hold the details of the people who have requested the service in order
    to provide it. However, we only use these details to provide the service the person
    has requested and for other closely related purposes. For example, we might use
    information about people who have signed up to our newsletter to carry out a survey
    to find out if they are happy with the content they receive. It will not be disclosed
    or shared for any other purpose. When people do subscribe to our services, they
    can cancel their subscription at any time and are given an easy way of doing this.
  header: People who use Snook services
- template: generic-content-block
  copy: We use a third party provider, MailChimp, to deliver our monthly e-newsletters.
    We gather statistics around email opening and clicks using industry standard technologies
    to help us monitor and improve our e-newsletter. For more information, please
    see [**MailChimp’s privacy notice**](https://mailchimp.com/legal/privacy/).
  header: People who sign up for our e-newsletter
- template: generic-content-block
  header: Access to your data
  copy: |-
    Under the General Data Protection Regulation individuals have the right to make a request for information, rectification, erasure, restriction, transfer, and/or object to the processing of their data. [**Find out more about your rights on the Information Commissioner’s Office website.**](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/)
    A request for any of the above can be done verbally or in writing by using [**our contact details**](https://wearesnook.com/contact/). In order to fulfil the request we must be able to verify your identity and may ask for further details about you and your contact with Snook. We will respond to a request within one calendar month of receiving it, free of charge. However, if the request is complex or there are numerous requests we may extend this deadline by a further two months and/or charge a fee, we will notify you if this is the case and explain why.
- template: generic-content-block
  copy: Snook tries to meet the highest standards when collecting and using personal
    information. For this reason, we take any complaints we receive about this very
    seriously. We encourage people to bring it to our attention by using [**our contact
    details **](https://wearesnook.com/contact/)if they think that our collection
    or use of information is unfair, misleading, or inappropriate.
  header: Complaints or queries
menu:
  sub_nav:
    weight: 2

---
