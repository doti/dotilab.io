---
title: Networking
description: Networking page
date: 2018-05-01 11:44:33 +0000
slug: breakfasts
type: section
location: ''
city: ''
startTime: ''
endTime: ''
bookingURL: ''
speakerList:
  speakers: []
  isComplete: false
reflection:
  image: ''
  text: ''
  links: []
host_title: Our host
host_copy: |-
  Sarah is a designer, CEO and serial idea generator. She co-founded [Snook](https://wearesnook.com/), [MyPolice](https://www.theguardian.com/society/2010/apr/28/1), [CycleHack](https://www.cyclehack.com/), [Dearest Scotland](http://www.dearestscotland.com/), [Alloa Pride](https://www.facebook.com/events/607945536037633/) and [The Matter](https://www.facebook.com/thisisthematter/?fref=ts).

  She was awarded a Google Fellowship for her work in technology and democratic innovation and named as [Good magazine’s 100](https://www.good.is/features/issue-36-the-good-100-full-list) extraordinary individuals tackling global issues in creative ways.

  Sarah is the CEO of [Snook](https://wearesnook.com/).

---
