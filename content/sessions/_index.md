---
title: Sessions
description: Our DOTI sessions run bi-monthly and bring together a cross section of
  speakers across a topic or sector. Filled with morning pastries, we host an open
  and honest dialogue on what it’s really like to Design on the Inside.
date: 2018-05-01 11:44:33 +0000
slug: sessions
type: section
host_title: Our host
host_copy: |-
  Sarah is a designer, CEO and serial idea generator. She co-founded [Snook](https://wearesnook.com/), [MyPolice](https://www.theguardian.com/society/2010/apr/28/1), [CycleHack](https://www.cyclehack.com/), [Dearest Scotland](http://www.dearestscotland.com/), [Alloa Pride](https://www.facebook.com/events/607945536037633/) and [The Matter](https://www.facebook.com/thisisthematter/?fref=ts).

  She was awarded a Google Fellowship for her work in technology and democratic innovation and named as [Good magazine’s 100](https://www.good.is/features/issue-36-the-good-100-full-list) extraordinary individuals tackling global issues in creative ways.

  Sarah is the CEO of [Snook](https://wearesnook.com/).
host_links:
- title: Website
  url: http://rufflemuffin.org/
- url: https://twitter.com/rufflemuffin
  title: Twitter
ctas: []
menu:
  main:
    weight: 2

---
