---
title: Design on the Inside Launch
description: Keen to discuss designing on the inside of organisations, with the people
  who are doing it?
location: Snook, 18 London Lane, London, E8 3PR
reflection:
  blog:
    URL: ''
    linkText: ''
  image: "/uploads/2018/05/22/Event-1.jpg"
  text: Our first DOTI was launched as part of London’s Service Design festival. We
    brought together a cross section of charities, Government, housing associations
    and the world’s biggest retailer, Tesco to talk about what designing on the inside
    really looks like. We learnt that no matter what size you are, similar challenges
    exist. Language was a common theme, finding the right terms to talk about design
    that will translate across silos and disciplines.
  links: []
type: session
date: 2018-05-16 05:00:02 +0000
startTime: 2017-09-22 07:30:00 +0000
endTime: 2017-09-27 09:00:00 +0000
bookingURL: https://www.eventbrite.co.uk/e/designing-on-the-inside-snook-service-design-fringe-festival-tickets-37729011489#
speakerList:
  speakers:
  - firstName: Janet
    lastName: Thorne
    role: CEO
    company:
      name: Reach Skills
    bio: ''
    links: ''
  - firstName: Tracey
    lastName: Allen
    role: Senior Service Designer
    company:
      name: Thames Valley Housing Association
    bio: ''
    links: ''
  - firstName: John
    lastName: Baldwin
    role: Interim CEO
    company:
      name: Thames Valley Housing Association
    bio: ''
    links: ''
  - firstName: Kate
    lastName: Kapp
    role: Head of Service Design
    company:
      name: Tesco
    bio: ''
    links: ''
  isComplete: false
city: London

---
