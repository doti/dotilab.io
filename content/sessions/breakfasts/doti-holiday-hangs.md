---
title: DOTI Holiday Hangs
description: |-
  To celebrate our first full year of running the Design on the Inside event series we'd like to invite speakers, guests and general fans to some Holiday Hangs.

  Part way through the year we added a small entrance fee to our DOTI events to cover our delicious pastry treats. We've made a small profit on this and would like to give that back to our community. There will be a small bar tab so first come first served! Please do grab a ticket as we have a capacity limit on the venue.

  The evening will be a chance to feed into our reflection on the year so far, discuss DOTI 2019 and share your wishlist for the DOTI conference. Also a chance to meet fellow members of the community and, in true DOTI style, share your challenges and help each other.
location: Market Cafe, 2 Broadway Market  London  E8 4QJ
start_time: ''
end_time: ''
booking_URL: ''
publishdate: 
speaker_list:
  is_complete: false
  speakers:
  - name: ''
    role: ''
    company:
      name: ''
      URL: ''
    bio: ''
    links:
    - title: ''
      URL: ''
      is_default: false
    image: ''
reflection:
  copy: ''
  blog:
    URL: ''
    link_text: ''
  image: ''
city: London
type: session
date: 2018-11-22 10:27:23 +0000
startTime: 2018-12-12 18:30:00 +0000
endTime: 2018-12-12 22:00:00 +0000
price: ''
bookingURL: https://www.eventbrite.co.uk/e/doti-holiday-hangs-tickets-52494408188
speakerList:
  speakers: []
  isComplete: false

---
