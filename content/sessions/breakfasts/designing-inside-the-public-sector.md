---
title: Designing inside the public sector
description: |-
  Design on the inside is back, and this time — we’re north of the border.

  We are excited to announce that we will be hosting the first ever Glasgow-based DOTI this February; with a focus on designing on the inside of the public sector.

  Service design is now at the stage where organisations need to embrace and practice design from the inside. However, we know there are tough challenges ahead, especially when it comes to public services. From dissecting the difficulties of digital transformation to picking apart the issue of senior management buy-in.
location: The Lighthouse  11 Mitchell Lane  Glasgow  G1 3NU
start_time: ''
end_time: ''
booking_URL: ''
publishdate: 
speaker_list:
  is_complete: false
  speakers:
  - name: ''
    role: ''
    company:
      name: ''
      URL: ''
    bio: ''
    links:
    - title: ''
      URL: ''
      is_default: false
    image: ''
reflection:
  copy: ''
  blog:
    URL: ''
    link_text: ''
  image: ''
city: Glasgow
type: session
startTime: 2019-02-01 09:00:00 +0000
endTime: 2019-02-01 10:30:00 +0000
price: ''
bookingURL: https://www.eventbrite.co.uk/e/doti-designing-inside-the-public-sector-glasgow-tickets-54910661266
speakerList:
  speakers:
  - firstName: Dr Maria
    lastName: Maclennan
    role: Academic Research Lead
    company:
      name: Police Scotland
    bio: ''
    links: ''
  - firstName: Alison
    lastName: Turnbull
    role: Director of Development and Partnership
    company:
      name: Historic Environment Scotland
    bio: ''
    links: ''
  - firstName: 'Jane '
    lastName: Reid
    role: User Research Consultant
    company:
      name: Freelance contractor
    bio: ''
    links: ''
  - firstName: Sam
    lastName: Ernstzen
    role: Service Designer
    company:
      name: Scottish Government
    bio: ''
    links: ''
  isComplete: true
date: 2019-01-15 17:05:10 +0000
draft: true

---
