---
title: 'Charities: from frontline to funding'
description: When you’ve got to be thinking about funders, frontline staff, end users
  and the needs of the organisation to juggle, how do you make decisions about service
  delivery with confidence?
location: Snook, 18 London Lane, London, E8 3PR
reflection:
  blog:
    URL: ''
    linkText: ''
  image: "/uploads/2018/05/22/Event-4.jpg"
  text: Our 4th DOTI focused on Charities. When you’ve got to be thinking about funders,
    frontline staff, end users and the needs of the organisation to juggle, how do
    you make decisions about service delivery with confidence? We heard from The Big
    Lottery, Addaction, Age UK and Wycombe Homeless Connection. We were told that
    digital has been a vehicle for thinking about the transformation of services,
    and that it’s hard to make time when you’re at the frontline for redesigning services.
  links: []
type: session
date: 2018-05-16 04:54:33 +0000
startTime: 2018-04-06 07:30:00 +0000
endTime: 2018-04-06 09:00:00 +0000
bookingURL: https://www.eventbrite.co.uk/e/from-frontline-to-funding-design-from-the-perspective-of-a-charity-tickets-43780153618#
speakerList:
  speakers:
  - firstName: Naomi
    lastName: Sanders
    role: Digital Project Manager
    company:
      name: Age UK
    bio: ''
    links: ''
  - firstName: Rosie
    lastName: Mockett
    role: Portfolio Policy Manager
    company:
      name: Big Lottery
    bio: ''
    links: ''
  - firstName: Bryony
    lastName: Albery
    role: Senior Support Worker
    company:
      name: Wycombe Homeless Connection
    bio: ''
    links: ''
  - firstName: Laura
    lastName: Bunt
    company:
      name: Addaction
    bio: ''
    links: ''
    role: Executive Director of Business Development
  isComplete: true
city: London

---
