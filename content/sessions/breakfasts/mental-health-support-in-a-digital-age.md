---
title: Mental health support in a digital age
description: "Mental health is finally starting to get the attention it deserves.
  At the same time, the way we use and deliver services is changing, rapidly. Charities
  are transforming their services to meet the ever changing needs of their users.
  Innovative approaches to self help are flooding our consciousness. And all this
  against the backdrop of an underfunded and under resourced NHS. There has never
  been a more poignant time to debate the suitability, expectations and possibilities
  of digital to aid in the provision of mental health support. \n\nThis month, DOTI
  brings together charities, innovators and researchers. Exploring the journey from
  academic research, through wellbeing and crisis to measuring the impact of our interventions. "
location: Snook, 18 London Lane, London, E8 3PR
start_time: ''
end_time: ''
booking_URL: ''
publishdate:
speaker_list:
  is_complete: false
  speakers:
  - name: ''
    role: ''
    company:
      name: ''
      URL: ''
    bio: ''
    links:
    - title: ''
      URL: ''
      is_default: false
    image: ''
reflection:
  copy: ''
  blog:
    URL: ''
    link_text: ''
  image: ''
city: London
type: session
date: 2018-10-08 10:39:22 +0000
startTime: 2018-11-09 08:30:00 +0000
endTime: 2018-11-09 10:00:00 +0000
price: £5
bookingURL: https://www.eventbrite.co.uk/e/mental-health-support-in-a-digital-age-tickets-51136922913
speakerList:
  speakers:
  - firstName: 'Ivor '
    lastName: Williams
    role: Co-founder
    bio: ''
    links: ''
    company:
      name: Humane Engineering
      URL: http://humaneengineering.com/
  - firstName: 'Simon '
    lastName: Stewart
    role: Senior Product Manager
    company:
      name: Samaritans
      URL: https://www.samaritans.org/
    bio: ''
    links: ''
  - firstName: 'Hannah '
    lastName: Pidsley
    role: Contracts Manager
    company:
      name: ThinkAction
      URL: https://www.thinkaction.org.uk/
    bio: ''
    links: ''
  isComplete: false

---
