---
title: Public sector
description: Service design is now at the stage where organisations need to embrace
  and practice design from the inside. However, we know there are tough challenges
  ahead. Ever wondered why Dan's Digital Transformation programme never took off?
  Or why 'investing in innovation' never landed that 'big idea' everyone thought it
  would? Or why 'getting stuff done' can feel like an uphill struggle when you're
  navigating constantly restructured silos and year-long backlogs? We take a look
  at designing on the inside of organisations with the people who are doing it, this
  time with a focus on design inside Government.
location: Snook, 18 London Lane, London, E8 3PR
reflection:
  blog:
    URL: ''
    linkText: ''
  image: "/uploads/2018/05/22/Event-2.jpg"
  text: 'We heard from Big Society Capital, the Home Office, Waltham Forest Council
    and Policy Lab. As part of a public sector focused DOTI, we celebrated some of
    the fantastic work taking place in governmental institutions at all levels. From
    design being used to support open policy making, as a central capacity inside
    ministerial bodies to supporting local authorities to commission services with
    a user centered focus. A big lesson, in environments where design isn’t naturally
    found was to just keep going. '
  links: []
type: session
date: 2018-05-16 04:42:37 +0000
startTime: 2017-11-17 08:30:00 +0000
endTime: 2017-11-17 10:00:00 +0000
bookingURL: https://www.eventbrite.co.uk/e/design-on-the-inside-tickets-39088699352#
speakerList:
  speakers:
  - firstName: Ayesha
    lastName: Moarif
    company:
      name: Digital Home Office
    bio: ''
    links: ''
    role: Service Design Lead
  - firstName: Mia
    lastName: Peters
    role: Service Design Lead
    company:
      name: Waltham Forest
    bio: ''
    links: ''
  - firstName: Beatrice
    lastName: Andrews
    role: Head of Policy Lab
    company:
      name: Cabinet Office
    bio: ''
    links: ''
  - firstName: Kieran
    lastName: Whiteside
    role: Project Manager
    company:
      name: Big Society Capital
    bio: ''
    links: ''
  isComplete: true
city: London

---
