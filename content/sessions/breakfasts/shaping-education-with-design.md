---
title: Shaping education with design
description: "This time, we are focusing on education.\n\nFrom discussing the design
  of education at scale, to exploring how you teach from a pirate ship - we look forward
  to welcoming our panel of teachers, and education radicals to discuss how this ever
  changing landscape can be positively influenced by design. "
location: Snook, 18 London Lane, London, E8 3PR
reflection:
  blog:
    URL: ''
    linkText: ''
  image: ''
  text: ''
  links: []
type: session
date: 2018-05-16 07:18:15 +0000
startTime: 2018-06-29 08:30:00 +0000
endTime: 2018-06-29 10:00:00 +0000
speakerList:
  speakers:
  - firstName: Tess
    lastName: Doughty
    role: Teacher
    company:
      name: St. Dominic’s Primary School, Homerton
    bio: ''
    links: ''
  - firstName: Darius
    lastName: Pocha
    role: ''
    company:
      name: DfE
    bio: ''
    links: ''
  - firstName: Andrew
    lastName: McWhirter
    role: Teacher
    company:
      name: Hackney Pirates
      URL: http://www.hackneypirates.org/
    bio: ''
    links:
    - title: Hackney Pirates
      URL: http://www.hackneypirates.org/
      isPrimary: true
  - firstName: Jim
    lastName: Ralley
    role: Co-Founder
    company:
      name: Flux
      URL: http://www.flux.am/
    bio: ''
    links: ''
  - firstName: Zahra
    lastName: Davidson
    role: Founder
    company:
      name: Enrol Yourself
      URL: https://www.enrolyourself.com/
    bio: ''
    links:
    - title: Enrol Yourself
      URL: https://www.enrolyourself.com/
      isPrimary: true
  isComplete: true
bookingURL: https://www.eventbrite.co.uk/e/doti-shaping-education-with-design-tickets-46687725243
city: London
price: £5.00

---
