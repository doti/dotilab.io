---
title: Needs, needs, needs
description: When you’ve got the needs of your users screaming in one ear, and the
  needs of the business echoing in the other, how do you make decisions with confidence
  and balance objectives? As designers, we juggle the pull of different needs in our
  day to day. However when you wrestle these forces within the realities of service
  delivery, who comes out on top?
location: Snook, 18 London Lane, London, E8 3PR
start_time: ''
end_time: ''
booking_URL: ''
speaker_list:
  is_complete: false
  speakers:
  - name: ''
    role: ''
    company:
      name: ''
      URL: ''
    bio: ''
    links:
    - title: ''
      URL: ''
      is_default: false
    image: ''
reflection:
  copy: ''
  blog:
    URL: ''
    link_text: ''
    linkText: ''
  image: "/uploads/2018/05/22/Event-3.jpg"
  text: We talked Needs, needs, needs with Skyscanner, Government Digital Service,
    Hive and Lloyds banking group. When you’ve got the needs of your users screaming
    in one ear,  and the needs of the business echoing in the other, how do you make
    decisions with confidence and balance objectives? Our panel discussion revolved
    around understanding return on investment of a business when using design and
    thinking about the wider system of where your design decisions will impact.
  links:
  - title: ''
    isPrimary: ''
    URL: ''
type: session
date: 2018-05-16 04:49:03 +0000
startTime: 2018-01-19 08:30:00 +0000
endTime: 2018-01-19 10:00:00 +0000
bookingURL: https://www.eventbrite.co.uk/e/needs-needs-needs-tickets-40092301152#
speakerList:
  speakers:
  - firstName: Nicola
    lastName: Combe
    role: Global Product Lead
    company:
      name: Hive
    bio: ''
    links: []
    image: "/uploads/2018/05/16/BW5T9771.JPG"
  - firstName: Louise
    lastName: Down
    company:
      name: Government Digital Service
    bio: ''
    links: ''
    role: Director of Design
  - firstName: Alberta
    lastName: Soranzo
    role: Service Design Director
    company:
      name: Lloyds Banking Group
    bio: ''
    links: ''
  - firstName: Buzz
    lastName: Pearce
    role: Head of Design
    company:
      name: Skyscanner
    bio: ''
    links: ''
  isComplete: true
city: London

---
