---
title: Housing in crisis - how can design help?
description: |+
  This time we're focusing on housing and the crisis we're currently facing in the UK, with a focus on London.

  This is a topic close to a lot of people's hearts. It's also a complex, systemic issue. We're bringing a variety of angles and perspectives to the discussion in order to really dig into how design can be used for positive intervention. We will be looking at it from a systemic level, in areas like planning and development, and through innovative approaches, in areas like homelessness prevention and community engagement.

location: Snook, 18 London Lane, London, E8 3PR
start_time: ''
end_time: ''
booking_URL: ''
publishdate:
speaker_list:
  is_complete: false
  speakers:
  - name: ''
    role: ''
    company:
      name: ''
      URL: ''
    bio: ''
    links:
    - title: ''
      URL: ''
      is_default: false
    image: ''
reflection:
  copy: ''
  blog:
    URL: ''
    link_text: ''
  image: ''
  text: ''
  links: []
type: session
date: 2018-07-27 15:55:53 +0000
startTime: 2018-08-17 08:30:00 +0000
endTime: 2018-08-17 10:00:00 +0000
bookingURL: https://www.eventbrite.co.uk/e/housing-in-crisis-tickets-48377488370
speakerList:
  speakers:
  - firstName: Jay
    lastName: Mistry
    role: ''
    company:
      name: National Housing Federation
    bio: ''
    links: ''
  - firstName: Louise
    lastName: Wyman
    role: ''
    company:
      name: Homes England
    bio: ''
    links: ''
  - firstName: Alastair
    lastName: Parvin
    company:
      name: Open Systems Lab
    bio: ''
    links: ''
  - firstName: Chris
    lastName: Hildrey
    role: ''
    company:
      name: Proxy Address
    bio: ''
    links: ''
  - firstName: Jon
    lastName: Foster
    role: ''
    bio: ''
    links: ''
    company:
      name: Housing camp
  isComplete: true
city: London
price: £5.00

---
