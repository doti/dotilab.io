document.addEventListener('DOMContentLoaded', app(), false);

function app() {
  openNav();
  closeNav();
  toggleCollapse();
  randomBGColor()
}

function openNav() {
    var menu = document.getElementById("menu");
    menu.addEventListener('click', function(event) {
      event.preventDefault();
      var mainMenu = document.getElementById("main-menu");
      // show the close-menu element.
      menu.style.display = 'none';
      mainMenu.style.display = 'block';
    });
};

function closeNav() {
  // target the close-menu element.
  var close = document.getElementById("close-menu");
  close.addEventListener('click',function(event) {
    event.preventDefault();
    var mainMenu = document.getElementById("main-menu");
    document.getElementById('menu').style.display = 'block';
    mainMenu.style.display = 'none';
  });
}

function randomChoice(arr) {
  var index = Math.floor(Math.random() * arr.length);
  return arr[index];
}


function toggleCollapse() {
  $('.read-close').click(function() {
    var rowSibling = $(this).siblings()[0];
    console.log(rowSibling, $(this).siblings())
    if (rowSibling.style.display != 'none') {
      rowSibling.style.display = 'none';
      $(this).find('.read-toggle').text('Read');
    } else {
      $(this).find('.read-toggle').text('Close');
      rowSibling.style.display = 'flex';
    }
  })
}

function randomBGColor() {
  let colors = [ "#FE4F2D",
    "#9AE0F4",
    "#FE103E",
    "#F8E71C",
    "#FFC8FE",
    "#35DEA6",
    "#4F4DDB",
    "#EFEFEF",
    "#FF95FF"
  ]

  $('.session-card').each(function(card) {
    $(this).css({ "background-color": colors[Math.floor(Math.random() * colors.length)] })
  })
}
